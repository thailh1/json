import React from 'react'
import styled from 'styled-components'
import PhoneInput from 'react-phone-number-input'
import Select from 'react-select'
import {country} from "../country";
import {language} from "../language";
import Organization from '../Containers/Organization'
import Switch from "react-switch";
import {Button} from 'reactstrap';
/*
* @function RichListInput
* @param Input component, onChange function to update store data, array of value
* @return list of inputs elements with value, auto add new input field
*
* */

// Select contactType component
const ContactType = ({value, update, field, data, index}) => {
    return <select value={value} onChange={e => {
        let a = [...data]
        a[index][field] = e.target.value
        update(a)
    }
    }>
        <option value="customer service ">customer service</option>
        <option value="technical support">technical support</option>
        <option value="billing support">billing support</option>
        <option value="bill payment">bill payment</option>
        <option value="sales">sales</option>
        <option value="reservations">reservations</option>
        <option value="credit card support">credit card support</option>
        <option value="emergency">emergency</option>
        <option value="baggage tracking">baggage tracking</option>
        <option value="roadside assistance">roadside assistance</option>
        <option value="package tracking">package tracking</option>
    </select>
}


/*
* Select areaServed
* */
const Country = ({value = [], update, field, index, data}) => {
    let val = country.filter(e => value.includes(e.value))
    return (
        <>
            <Switch uncheckedIcon={false}
                    checkedIcon={false} height={16}
                    width={32} handleDiameter={16}
                    checked={data[index].hasOwnProperty(field)}
                    onChange={(e) => {
                        let a = [...data]
                        if (e) {
                            a[index][field] = []
                            update(a)
                        } else {
                            delete a[index][field];
                            update(a)
                        }
                    }}
            />
            <Select value={val} options={country}
                    isMulti
                    isSearchable
                    styles={{
                        control: (provided) => ({
                            ...provided,
                            width: '700px',
                        }),
                    }}
                    onChange={e => {
                        if (!Array.isArray(e) || !e.length) {
                            let a = [...data]
                            a[index][field] = []
                            update(a)
                        } else {
                            let country = e.map(e => e.value) || [];
                            let a = [...data]
                            a[index][field] = country
                            update(a)
                        }

                    }}
            />
        </>

    );
};

/*
* Select availableLanguage
* */
const CountryLabel = ({value = ['English'], update, field, index, data}) => {
    let val = language.filter(e => value.includes(e.label))
    return (
        <>
            <Switch uncheckedIcon={false}
                    checkedIcon={false} height={16}
                    width={32} handleDiameter={16}
                    checked={data[index].hasOwnProperty(field)}
                    onChange={(e) => {
                        let a = [...data]
                        if (e) {
                            a[index][field] = ['English']
                            update(a)
                        } else {
                            delete a[index][field];
                            update(a)
                        }
                    }}
            />
            <Select options={language}
                    isMulti
                    isSearchable
                    value={val}
                    styles={{
                        control: (provided) => ({
                            ...provided,
                            width: '700px',
                        }),
                    }}

                    onChange={e => {
                        if (!Array.isArray(e) || !e.length) {
                            let a = [...data]
                            a[index][field] = []
                            update(a)
                        } else {
                            let country = e.map(e => e.label);
                            let a = [...data]
                            a[index][field] = country
                            update(a)
                        }

                    }}
            />
        </>

    );
};


const RichListInput = (data = [], update, field) => {

    const valuesList = data.map((item, index) => {
        return (<div key={index} style={{margin: 10, padding: 10}}>
                <div style={{margin: 10}} key={index}>
                    <PhoneInput data={data} value={item['telephone']} onChange={value => {
                        let a = [...data]
                        a[index]['telephone'] = value
                        update(a)
                    }}/>
                    <ContactType data={data} value={item['contactType']} field={'contactType'} index={index}
                                 update={update}/><br/>
                    <small style={{marginRight: 20}}>Areas served by this contact</small>
                    <Country data={data} value={item['areaServed']} field={'areaServed'} index={index} update={update}/>
                    <small style={{marginRight: 20}}>Select support language(s) for this contact. No selection will
                        default to English
                    </small>
                    <CountryLabel data={data} value={item['availableLanguage']} field={'availableLanguage'}
                                  index={index} update={update}/>
                </div>
                <Button color='danger' style={{height: 'fit-content'}}
                        onClick={() => Organization.removeInField(index, field)}>x
                </Button>

            </div>


        )
    })
    return <Wrapper>{valuesList}</Wrapper>
}
export default RichListInput

const Wrapper = styled.div`
      >div{
        display: flex;
        }
`

