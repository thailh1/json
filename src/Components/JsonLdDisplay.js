import React from 'react'
/*
* Preview JSON-LD
*
* */
const JsonLdDisplay = (jsonld) => {
    return (
        <pre className="pre-x-scrollable">
      {'<script type="application/ld+json">'}
            <br/>
            {JSON.stringify(jsonld, null, 4)}
            <br/>
            {'</script>'}
    </pre>
    )
}

export default JsonLdDisplay
