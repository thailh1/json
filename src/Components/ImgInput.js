import React from 'react'
import TextInput from './TextInput'
import validators from '../helpers/validators'

/***
 * Image URL input with validatopr
 *
 * */

const ImgInput = ({onChange, value, placeholder}) => {

    const valid = !value.length ? true : validators.isValidImage(value)
    console.log(!value.length)
    console.log(validators.isValidImage(value))
    console.log(valid)

    return (
        <>
            <div style={{color: 'red'}}>{valid ? null : 'Invalid image URL!'}</div>
            <TextInput
                valid={valid}
                value={value}
                placeholder={placeholder}
                onChange={onChange}
            />
        </>

    )
}

export default ImgInput
