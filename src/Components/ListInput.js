import React from 'react'
import {isEmpty, map} from 'lodash'
import styled from 'styled-components'
import {Button} from 'reactstrap';


/*
* @function ListInput
* @param Input component, onChange function to update store data, array of value
* @return list of inputs element with value, auto add new input field
*
* */

const ListInput = ({Input, onChange, placeholder, values = []}) => {


    // Add new value
    const handleChange = (value, index) => {
        let newValues = values.slice();
        if (!value) {
            newValues.splice(index, 1);
        } else {
            values.slice();
            newValues[index] = value;
        }
        newValues = isEmpty(newValues) ? undefined : newValues;
        return onChange(newValues);
    };

    const handleRemove = (index) => {
        return handleChange('', index);
    };

    // Render list input
    const valuesList = map(values.concat(['']), (value, index) => (
        <div>
            <div key={index}>
                <Input
                    value={value}
                    placeholder={placeholder}
                    onChange={value => handleChange(value, index)}
                />

            </div>
            {value ? (
                <Button color="danger" size="sm" style={{height: 'fit-content', margin: 'auto 0px'}}
                        onClick={() => handleRemove(index)}>
                    X
                </Button>
            ) : (
                ''
            )}
        </div>

    ))

    return <Wrapper>{valuesList}</Wrapper>
}

export default ListInput

const Wrapper = styled.div`
  >div{
    display: flex;
    flex-direction:row;
}

`

//