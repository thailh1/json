import React from 'react'
import TextInput from './TextInput'
import validators from '../helpers/validators'

/***
 *  URL input with validatopr
 *
 * */
const UrlInput = ({onChange, value, placeholder}) => {

    const valid = !value.length ? true : validators.isValidUrl(value)
    console.log(!value.length)
    console.log(validators.isValidUrl(value))
    console.log(valid)

    return (
        <>
            <div style={{color: 'red'}}>{valid ? null : 'Invalid URL!'}</div>
            <TextInput
                valid={valid}
                value={value}
                placeholder={placeholder}
                onChange={onChange}
            />
        </>

    )
}

export default UrlInput
