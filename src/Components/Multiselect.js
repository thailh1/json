import React from 'react'
import {Multiselect} from 'react-widgets'

/*
* Multi-select component, data is a list of object with label for display and value to handle
* */

const multiselect = (data, value = [], onChange) => {
    return <Multiselect
        dropDown
        data={data}
        value={value}
        onChange={onChange}
    />
}

export default multiselect;