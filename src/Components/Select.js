import React from 'react'
import { map } from 'lodash'

const Select = ({ onChange, options }) => {
  return (
    <input type="select" onChange={e => onChange(e.target.value)}>
      {map(options, option => (
        <option key={option.id} value={option.id}>
          {option.name}
        </option>
      ))}
    </input>
  )
}

export default Select
