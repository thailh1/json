import React from "react";
import styled from "styled-components";

const TextInput = ({onChange, value, placeholder, valid = true, disabled}) => {
    const change = e => onChange && onChange(e.target.value);
    return (
        <InputWrapper valid={valid}>
            <input disabled={disabled} placeholder={placeholder} value={value} onChange={e => change(e)}/>
        </InputWrapper>
    );
};

export default TextInput;

const InputWrapper = styled.div`
  display: flex;
  min-width: 0;
  margin: 5px;
  input {
    width: 700px;
    background: transparent;
    min-width: 0;
    max-height: 100%;
    padding-left: 5px;
    border-radius: 5px;
    //width: 70%;
     border: 0.5px solid ${props => (props.valid ? "grey" : "red")};
    //border: 0.5px solid grey;
    line-height: 30px;
    flex: 1;
    font-size: 14px;
  > * {
    margin: auto 0 auto 8px;
    &:last-child {
      margin-right: 8px;
    }
    max-height: 100%;
  }
`;
