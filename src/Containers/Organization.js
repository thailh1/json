import {Container} from 'unstated';


// Organization data and updater

class Organization extends Container {

    state = {
        "@context": "http://schema.org",
        '@type': 'Organization',
    };
    updateField = field => {
        this.setState(Object.assign({}, this.state, {field}))
    }

    updateArray = (data, field) => {
        let a = this.state[field] = data
        this.setState(a)
    }
    addInField = (field, metadata) => {
        let a = {...this.state}
        a[field].push(metadata)
        this.setState(a)
    }
    removeInField = (index, field) => {
        let a = {...this.state}
        a[field].splice(index, 1)
        this.setState(a)

    }
    removeField = (field) => {
        let a = delete this.state[field];
        this.setState(a)
    }

    getString = () => {
        let myString = {...this.state}
        return JSON.stringify(myString)
    }
}

let a = new Organization()

window.Organization = a;

export default a