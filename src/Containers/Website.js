import {Container} from 'unstated';

// Website data

class Website extends Container {
    state = {
        'enable': true,

    }
    getString = () => {
        let {enable} = this.state
        if (enable) {
            return JSON.stringify(this.state.website)

        } else return ''
    }
    getDisplay = () => {

        return this.state.website

    }
    init = (domain) => {
        this.setState({
            website: {
                "@context": "http://schema.org",

                '@type':
                    'WebSite',
                'url':
                    `https://${domain}`,
                potentialAction:
                    {
                        '@type':
                            "SearchAction",
                        'target':
                            `https://${domain}/search?q={search_term_string}`,
                        'query-input':
                            'required name=search_term_string'
                    }
            }
        })

    }

}

let website = new Website()

window.Website = website;

export default website