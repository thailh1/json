import {Container} from 'unstated';

// Product setting

class Product extends Container {
    state = {
        'enable': false,
        'sku': false,
        'identifier': '',
        'brand': false,
        'rating': false,
        'reviews': false
    }
    getString = () => {
        return JSON.stringify(this.state)
    }
}

let a = new Product()

window.Product = a;

export default a