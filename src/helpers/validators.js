/*
* Validator for URL and image URL
* check if input value is URL and image URL
* */


const validators = {
    isValidUrl: (value) => {

        let a = document.createElement('a');
        a.href = value;
        return !!(a.host && a.host !== window.location.host);
    },
    isValidImage: (value) => {

        let a = document.createElement('a');
        a.href = value;
        return !!(a.host && a.host !== window.location.host && value.match(/\.(jpg|png)$/) != null);
    }
};

export default validators;
