import React from 'react'
import TextInput from './Components/TextInput'
import JsonLdDisplay from "./Components/JsonLdDisplay";
import ListInput from "./Components/ListInput";
import RichListInput from "./Components/RichListInput";
import 'react-phone-number-input/style.css'
import "./App.css"
import Organization from './Containers/Organization'
import Product from './Containers/Product'
import Website from './Containers/Website'
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import {Tabs, Tab} from 'react-tab-view'
import styled from 'styled-components'
import Switch from "react-switch";
import {Subscribe} from "unstated";
import isArray from 'lodash/isArray'
import {Button} from 'reactstrap';
import UrlInput from "./Components/UrlInput";
import ImgInput from "./Components/ImgInput";

const $Test = styled.div`
  >div{
     border: unset;
     > div{
    cursor: pointer;}
}
`
const updateArray = (data) => {
    let a = Object.assign({}, {contactPoint: data})
    Organization.setState(a)
}


class App extends React.Component {

    componentWillMount() {
        const a = document.querySelector('#root')
        this.setState({api: a.dataset.api})
    }

    state = {
        api: 'https://4963aa2f.ngrok.io/json'
    }

    componentDidMount() {
        /*
        *
        * fetch data from database if not exist init data from shopify
        * */
        fetch(`${this.state.api}/settings.php`)
            .then(res => res.json())
            .then(res => {
                if (res) {
                    if (res.organization['@type']) {
                        let result = res.organization
                        Organization.setState(result)
                        Website.init(res.shop_domain)
                        console.log(res.product)
                        Product.setState(res.product)

                    } else {
                        fetch(`${this.state.api}/shopinfo.php`)
                            .then(res => res.json())
                            .then(res => {
                                    console.log(res)
                                    Organization.setState({
                                        name: res.name,
                                        url: `https://${res.domain}`,
                                    })
                                    Website.init(res.domain)
                                }
                            )
                    }
                }
            })

    }

    render() {
        const headers = ['Organization', 'Contact Point', 'WebSite', 'Products']

        return (
            <div>
                <ToastContainer/>
                <div>JSON Generator</div>

                <Subscribe to={[Organization]}>
                    {
                        (container) => (
                            <div style={{display: 'flex'}}>
                                <$Test style={{width: '50%'}}>

                                    <Tabs style={{border: 'unset'}} headers={headers}>

                                        /*
                                        * Organization tab
                                        * */
                                        <Tab>
                                            <div>

                                                <TextInput placeholder="Name" value={container.state.name}
                                                           onChange={name => Organization.setState(Object.assign({}, Organization.state, {name}))}/>
                                                <small style={{marginRight: 20}}>Input URL of your store logo. It’s
                                                    recommended that you use a file
                                                    already in use
                                                    by your
                                                    store
                                                    for greater crawlability
                                                </small>
                                                <Switch uncheckedIcon={false}
                                                        checkedIcon={false} height={16}
                                                        width={32} handleDiameter={16}
                                                        checked={container.state.hasOwnProperty('logo')}
                                                        onChange={(e) => {
                                                            if (e) {
                                                                Organization.setState({'logo': ''})
                                                            } else {
                                                                Organization.removeField('logo')
                                                            }
                                                        }}
                                                />
                                                <>{
                                                    container.state.hasOwnProperty('logo') ?
                                                        <ImgInput placeholder="Logo"
                                                                  value={container.state.logo}
                                                                  onChange={logo => {
                                                                      if (container.state.hasOwnProperty('logo')) {
                                                                          Organization.setState(Object.assign({}, Organization.state, {logo}))
                                                                      }
                                                                  }}/> : null
                                                }</>


                                                <TextInput placeholder="Url" value={container.state.url}
                                                           onChange={url => Organization.setState(Object.assign({}, Organization.state, {url}))}/>
                                                <small style={{marginRight: 20}}>Social media profile URLs. The below
                                                    platforms are recognized by
                                                    google search
                                                </small>
                                                <Switch uncheckedIcon={false}
                                                        checkedIcon={false} height={16}
                                                        width={32} handleDiameter={16}
                                                        checked={isArray(container.state.sameAs)}
                                                        onChange={(e) => {
                                                            if (e) {
                                                                Organization.setState({'sameAs': []})
                                                            } else {

                                                                Organization.removeField('sameAs')

                                                            }
                                                        }}
                                                />
                                                <>{
                                                    isArray(container.state.sameAs) ? <ListInput
                                                        id="orgSameAs"
                                                        values={container.state.sameAs}
                                                        placeholder="Same As"
                                                        Input={UrlInput}
                                                        onChange={sameAs => {
                                                            if (isArray(container.state.sameAs)) {
                                                                Organization.setState(Object.assign({}, Organization.state, {sameAs}))
                                                            }
                                                        }
                                                        }
                                                    /> : null
                                                }</>


                                            </div>
                                        </Tab>
                                        <Tab>
                                            /*
                                             * Contact tab
                                             * */
                                            <small style={{marginRight: 20}}>Corporate Contact allows you to submit
                                                phone
                                                contact details for your
                                                store
                                            </small>
                                            <Switch uncheckedIcon={false}
                                                    checkedIcon={false} height={16}
                                                    width={32} handleDiameter={16}
                                                    checked={isArray(container.state.contactPoint)}
                                                    onChange={(e) => {
                                                        if (e) {
                                                            Organization.setState({'contactPoint': []})
                                                        } else {
                                                            Organization.removeField('contactPoint')

                                                        }
                                                    }}
                                            />
                                            <div>{RichListInput(container.state.contactPoint, updateArray, 'contactPoint')}</div>
                                            <Button color="info" onClick={() => {
                                                let a = {...container.state}
                                                if (isArray(container.state.contactPoint)) {
                                                    a.contactPoint.push({
                                                        "@type": "ContactPoint",
                                                        "telephone": "",
                                                        "contactType": "customer service",

                                                    })
                                                    Organization.setState(a)
                                                }

                                            }}>Add contact
                                            </Button>
                                        </Tab>
                                        <Tab>
                                            /*
                                       * Website tab
                                       * */
                                            <Subscribe to={[Website]}>
                                                {
                                                    website => (
                                                        <>
                                                            <small style={{marginRight: 20}}>Toggle to provide google
                                                                with details
                                                                to
                                                                display a search box on certain
                                                                search
                                                                result pages
                                                            </small>
                                                            <Switch uncheckedIcon={false}
                                                                    checkedIcon={false}
                                                                    checked={website.state.enable}
                                                                    height={16}
                                                                    width={32} handleDiameter={16}
                                                                    onChange={(e) => {

                                                                        Website.setState({enable: e})

                                                                    }}

                                                            />
                                                            <div>{JsonLdDisplay(website.getDisplay())}
                                                            </div>
                                                        </>
                                                    )
                                                }

                                            </Subscribe>

                                        </Tab>
                                        <Tab>
                                            /*
                                            * Product Tab
                                            * */
                                            <Subscribe to={[Product]}>
                                                {
                                                    product => (
                                                        <>
                                                            <div>Product Structured Data</div>
                                                            <small style={{marginRight: 20}}>Provide pricing, reviews
                                                                and more product
                                                                data
                                                                that Google can highlight
                                                                in rich
                                                                snippets and SERP results.
                                                            </small>
                                                            <Switch uncheckedIcon={false} checkedIcon={false}
                                                                    checked={product.state.enable} height={16}
                                                                    width={32} handleDiameter={16}
                                                                    onChange={e => Product.setState({enable: e})}
                                                            />
                                                            <div>sku</div>
                                                            <Switch uncheckedIcon={false} checkedIcon={false}
                                                                    checked={product.state.sku} height={16}
                                                                    width={32} handleDiameter={16}
                                                                    onChange={e => Product.setState({sku: e})}
                                                            />
                                                            <div>identifier</div>

                                                            <Switch uncheckedIcon={false} checkedIcon={false}
                                                                    checked={product.state.identifier.length !== 0}
                                                                    height={16}
                                                                    width={32} handleDiameter={16}
                                                                    onChange={e => {
                                                                        e ? Product.setState({identifier: ' '})
                                                                            : delete Product.state.identifier
                                                                    }}

                                                            />
                                                            <div>
                                                                {
                                                                    product.state.identifier ? <select
                                                                            onChange={e => Product.setState({identifier: e.target.value})}>
                                                                            <option> mpn</option>
                                                                            <option> gtin8</option>
                                                                            <option> gtin13</option>
                                                                            <option> gtin14</option>
                                                                            <option> mpn</option>
                                                                            <option> Isbn</option>
                                                                        </select>

                                                                        : null
                                                                }
                                                            </div>

                                                            <div>brand</div>

                                                            < Switch uncheckedIcon={false} checkedIcon={false}
                                                                     checked={product.state.brand} height={16}
                                                                     width={32} handleDiameter={16}
                                                                     onChange={e => Product.setState({brand: e})}


                                                            />
                                                            <div>Aggregate Rating</div>
                                                            <Switch uncheckedIcon={false} checkedIcon={false}
                                                                    checked={product.state.rating} height={16}
                                                                    width={32} handleDiameter={16}
                                                                    onChange={e => Product.setState({rating: e})}

                                                            />
                                                            <div>Reviews</div>
                                                            <Switch uncheckedIcon={false} checkedIcon={false}
                                                                    checked={product.state.reviews} height={16}
                                                                    width={32} handleDiameter={16}
                                                                    onChange={e => Product.setState({reviews: e})}

                                                            /></>
                                                    )
                                                }

                                            </Subscribe>
                                        </Tab>
                                    </Tabs>
                                </$Test>

                                <div style={{width: '30%', marginLeft: 30}}>
                                    <Button outline
                                            color="primary"
                                            onClick={() => {
                                                fetch(`${this.state.api}/settings.php`, {
                                                    method: 'POST',
                                                    headers: {
                                                        'Content-Type': 'application/x-www-form-urlencoded'
                                                    },
                                                    body: `organization=${Organization.getString()}&product=${Product.getString()}&website=${Website.getString()}`
                                                })

                                                    .then(res => {
                                                        console.log(res)
                                                        toast.success('Saved')

                                                    }).catch(e => {
                                                    console.log(e)
                                                    toast.error('Error!')
                                                })
                                            }}>save</Button>
                                    <Button style={{margin: 10}} color="success"
                                            onClick={() => {
                                                fetch(`${this.state.api}/publish.php`, {
                                                    method: 'GET',
                                                    headers: {
                                                        'Content-Type': 'application/x-www-form-urlencoded'
                                                    },
                                                })
                                                    .then(res => {
                                                        console.log(res)
                                                        toast.success('Published')
                                                    }).catch(e => {
                                                    console.log(e)
                                                    toast.error('Error!')
                                                })
                                            }}>Publish</Button></div>

                            </div>
                        )
                    }
                </Subscribe>
            </div>
        )
    }

}

export default App
